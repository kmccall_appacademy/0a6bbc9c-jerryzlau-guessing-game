def shuffle_lines
  puts "Randomize a file by line and writes into new file."
  print "input file name: "
  file = gets.chomp
  name = file.split(".").first
  randomized = File.readlines(file).shuffle
  File.open("#{name}-shuffled.txt", "w") do |f|
    f.puts randomized
  end
end

shuffle_lines
